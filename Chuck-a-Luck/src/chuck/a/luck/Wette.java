/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chuck.a.luck;

/**
 *
 * @author Christoph Weiss
 */
public class Wette {
   
    private int einsatz_zahl1 = 0;
    private int einsatz_zahl2 = 0;
    private int einsatz_zahl3 = 0;
    private int einsatz_zahl4 = 0;
    private int einsatz_zahl5 = 0;
    private int einsatz_zahl6 = 0;
    private int einsatz_high = 0;
    private int einsatz_low = 0;
    private int einsatz_odd = 0;
    private int einsatz_even = 0;
    private int einsatz_summe = 0;
    private int einsatz_summe_hilfe = 0;
    private int einsatz_oddeven = 0;
    private int einsatz_oddevenAusgabe = 0;
    private int einsatz_highlow = 0;
    private int einsatz_highlowAusgabe = 0;
    private int w1 = 0;
    private int w2 = 0;
    private int w3 = 0;
    private int counterzahl1 = 0;
    private int counterzahl2 = 0;
    private int counterzahl3 = 0;
    private int counterzahl4 = 0;
    private int counterzahl5 = 0;
    private int counterzahl6 = 0;
    private int summezahl1_6 = 0;
    private int wergebnis = 0;
    private int gesamtgewinn = 0;
    private int summezahl1_6Ausgabe = 0;
    private int einsatz_summeAusgabe = 0;
    private int gesamtgewinnAusgabe = 0;
    private int gesamtEinsatz = 0;
    private int [] wuerfel = null;
    private GuiChuckaLuck wette = null;
    private Wuerfeln w = null;
    
    
    public Wette(GuiChuckaLuck asdf, Wuerfeln lol){
        wette = asdf;
        w= lol;
        
    }
    
    public void würfel(){
        w1 = wette.getWürfel1();
        w2 = wette.getWürfel2();
        w3 = wette.getWürfel3();
        wergebnis = wette.getErgebnis();
        wuerfel = new int[3];
        wuerfel[0] = w1;
        wuerfel[1] = w2;
        wuerfel[2] = w3;
    }
    
    
    public int getEinsatz_zahl2() {
        return einsatz_zahl2;
    }

    public void setEinsatz_zahl2(int einsatz_zahl2) {
        this.einsatz_zahl2 = einsatz_zahl2;
    }

    public int getEinsatz_zahl3() {
        return einsatz_zahl3;
    }

    public void setEinsatz_zahl3(int einsatz_zahl3) {
        this.einsatz_zahl3 = einsatz_zahl3;
    }

    public int getEinsatz_zahl4() {
        return einsatz_zahl4;
    }

    public void setEinsatz_zahl4(int einsatz_zahl4) {
        this.einsatz_zahl4 = einsatz_zahl4;
    }

    public int getEinsatz_zahl5() {
        return einsatz_zahl5;
    }

    public void setEinsatz_zahl5(int einsatz_zahl5) {
        this.einsatz_zahl5 = einsatz_zahl5;
    }

    public int getEinsatz_zahl6() {
        return einsatz_zahl6;
    }

    public void setEinsatz_zahl6(int einsatz_zahl6) {
        this.einsatz_zahl6 = einsatz_zahl6;
    }

    public int getEinsatz_high() {
        return einsatz_high;
    }

    public void setEinsatz_high(int einsatz_high) {
        this.einsatz_high = einsatz_high;
    }

    public int getEinsatz_low() {
        return einsatz_low;
    }

    public void setEinsatz_low(int einsatz_low) {
        this.einsatz_low = einsatz_low;
    }

    public int getEinsatz_odd() {
        return einsatz_odd;
    }

    public void setEinsatz_odd(int einsatz_odd) {
        this.einsatz_odd = einsatz_odd;
    }

    public int getEinsatz_even() {
        return einsatz_even;
    }

    public void setEinsatz_even(int einsatz_even) {
        this.einsatz_even = einsatz_even;
    }
   
    
    public int getEinsatz_zahl1() {
        return einsatz_zahl1;
    }

    public int getGesamtgewinn() {
        return gesamtgewinn;
    }
     public int getEinsatz_summe() {
        return einsatz_summe;
    }

    public int getGesamtgewinnAusgabe() {
        return gesamtgewinnAusgabe;
    }

    public void setGesamtgewinn(int gesamtgewinn) {
        this.gesamtgewinn = gesamtgewinn;
    }

    public void setGesamtgewinnAusgabe(int gesamtgewinnAusgabe) {
        this.gesamtgewinnAusgabe = gesamtgewinnAusgabe;
    }

    public void setGesamtEinsatz(int gesamtEinsatz) {
        this.gesamtEinsatz = gesamtEinsatz;
    }
    

    
    public void setEinsatz_zahl1(int einsatz_zahl) {
        this.einsatz_zahl1 = einsatz_zahl;
    }

    
    public void setEinsatz_summe(int einsatz_summe) {
        this.einsatz_summe = einsatz_summe;
    }
    
    
    public void checkFieldBet(){
        würfel();
         if(wergebnis ==  5 && wette.getCbk_Field5().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  6 && wette.getCbk_Field6().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  7 && wette.getCbk_Field7().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  8 && wette.getCbk_Field8().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  13 && wette.getCbk_Field13().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  14 && wette.getCbk_Field14().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  15 && wette.getCbk_Field15().isSelected() == true){
         einsatz_summeAusgabe += einsatz_summe;
         einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else if(wergebnis ==  16 && wette.getCbk_Field16().isSelected() == true){
           einsatz_summeAusgabe += einsatz_summe;
           einsatz_summe_hilfe += einsatz_summe + einsatz_summe;
       }else{
           einsatz_summeAusgabe = einsatz_summe_hilfe = 0;
       }
    }
    public void checkOddEven(){
        if(wergebnis % 2 == 0 && wette.getCkb_Even().isSelected() == true){
           einsatz_oddevenAusgabe += einsatz_even;
           einsatz_oddeven += einsatz_even+einsatz_even;
        }else if(wergebnis % 2 == 1 && wette.getCkb_Odd().isSelected() == true){
            einsatz_oddevenAusgabe += einsatz_odd;
            einsatz_oddeven += einsatz_odd+einsatz_odd;
        }else{
            einsatz_oddevenAusgabe = einsatz_oddeven = 0;
        }
           
    }
    public void checkOver10Under11(){
        if(wergebnis > 10  && wette.getCkb_Over10().isSelected() == true){
           einsatz_highlowAusgabe = einsatz_high;
           einsatz_highlow += einsatz_high + einsatz_high;
        }else if(wergebnis < 11 && wette.getCkb_Under11().isSelected() == true){
            einsatz_highlowAusgabe = einsatz_low;
            einsatz_highlow += einsatz_low+einsatz_low;
        }else{
            einsatz_highlowAusgabe = einsatz_highlow = 0;
        }
    }
   
    private void checkNumber1_6() {
        for (int i = 0; i < 3; i++) {
            if (wette.getCbk_1().isSelected() && wuerfel[i] == 1) {
                counterzahl1++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (wette.getCbk_2().isSelected() && wuerfel[i] == 2) {
                counterzahl2++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (wette.getCbk_3().isSelected() && wuerfel[i] == 3) {
                counterzahl3++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (wette.getCbk_4().isSelected() && wuerfel[i] == 4) {
                counterzahl4++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (wette.getCbk_5().isSelected() && wuerfel[i] == 5) {
                counterzahl5++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (wette.getCbk_6().isSelected() && wuerfel[i] == 6) {
                counterzahl6++;
            }
        }
        
        if(einsatz_zahl1>0 && counterzahl1>0){
        if (counterzahl1 == 3) {
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl1 * 10;
            summezahl1_6 += einsatz_zahl1;
        }else
            summezahl1_6Ausgabe += summezahl1_6 = einsatz_zahl1 * counterzahl1;
            summezahl1_6 += einsatz_zahl1;
        }else{
        }
        if(einsatz_zahl2>0 && counterzahl2>0){
        if (counterzahl2 == 3) {
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl2* 10;
            summezahl1_6 += einsatz_zahl2;
        }else
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl2 * counterzahl2;
            summezahl1_6 += einsatz_zahl2;
        }else{
        }
        if(einsatz_zahl3>0 && counterzahl3>0){
        if (counterzahl3 == 3) {
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl3* 10;
            summezahl1_6 += einsatz_zahl3;
        }else
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl3 * counterzahl3;
            summezahl1_6 += einsatz_zahl3;
        }else{
        }
        if(einsatz_zahl4>0 && counterzahl4>0){
        if (counterzahl4 == 3) {
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl4* 10;
            summezahl1_6 += einsatz_zahl4;
        }else
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl4 * counterzahl4;
            summezahl1_6 += einsatz_zahl4;
        }else{
         
        }
        if(einsatz_zahl5>0 && counterzahl5>0){
        if (counterzahl5 == 3) {
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl5* 10;
            summezahl1_6 += einsatz_zahl5;
        }else
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl5 * counterzahl5;
            summezahl1_6 += einsatz_zahl5;
        }else{
            
        }
        if(einsatz_zahl6>0 && counterzahl6>0){
        if (counterzahl6 == 3) {
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl6* 10;
            summezahl1_6 += einsatz_zahl6;
        }else
            summezahl1_6Ausgabe += summezahl1_6 += einsatz_zahl6 * counterzahl6;
            summezahl1_6 += einsatz_zahl6;
        }else{
            
        }
        
    }
    public int gesamtEinsatz(){
        return gesamtEinsatz = einsatz_zahl1+einsatz_zahl2+einsatz_zahl3+einsatz_zahl4+einsatz_zahl5+ einsatz_zahl6+einsatz_high+ einsatz_low+ einsatz_odd+ einsatz_even+ einsatz_summe; 
    }
    public void gesamtGewinn(){
        checkFieldBet();
        checkNumber1_6();
        checkOddEven();
        checkOver10Under11();
            gesamtgewinnAusgabe = einsatz_highlowAusgabe+einsatz_oddevenAusgabe+einsatz_summeAusgabe+summezahl1_6Ausgabe;
            gesamtgewinn = einsatz_summe_hilfe+einsatz_oddeven+einsatz_highlow+summezahl1_6;
   
    }
    public void ResetAll(){
    einsatz_zahl1 = 0;
    einsatz_zahl2 = 0;
    einsatz_zahl3 = 0;
    einsatz_zahl4 = 0;
    einsatz_zahl5 = 0;
    einsatz_zahl6 = 0;
    einsatz_high = 0;
    einsatz_low = 0;
    einsatz_odd = 0;
    einsatz_even = 0;
    einsatz_summe = 0;
    einsatz_summe_hilfe = 0;
    einsatz_oddeven = 0;
    einsatz_oddevenAusgabe = 0;
    einsatz_highlow = 0;
    einsatz_highlowAusgabe = 0;
    w1 = 0;
    w2 = 0;
    w3 = 0;
    counterzahl1 = 0;
    counterzahl2 = 0;
    counterzahl3 = 0;
    counterzahl4 = 0;
    counterzahl5 = 0;
    counterzahl6 = 0;
    summezahl1_6 = 0;
    wergebnis = 0;
    gesamtgewinn = 0;
    summezahl1_6Ausgabe = 0;
    einsatz_summeAusgabe = 0;
    gesamtgewinnAusgabe = 0;
    gesamtEinsatz = 0;
    }

   

    
   

}
