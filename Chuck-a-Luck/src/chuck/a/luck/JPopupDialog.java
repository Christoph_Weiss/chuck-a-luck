package chuck.a.luck;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author FabioH
 */
public class JPopupDialog extends JDialog implements Runnable
{
	private final long displaytime;

	private JPopupDialog(JFrame owner, String title, String msg, long displaytime, boolean modal) {
		super(owner, title, modal);
		this.displaytime = displaytime;
		setContentPane(new JLabel(msg, JLabel.CENTER));
		setLocationRelativeTo(owner);
		pack();
		setVisible(true);
		Thread t = new Thread(this, "Popup-Closer");
		t.start();
	}

	@Override
	public void run() {
		try {
			Thread.sleep(displaytime);
			dispose();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void create(JFrame owner, String title, String msg, long displaytime, boolean modal) {
		new JPopupDialog(owner, title, msg, displaytime, modal);
	}
	
	
}